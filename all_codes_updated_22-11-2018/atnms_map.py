import matplotlib.pyplot as plt
import time
import RPi.GPIO as GPIO
from time import sleep
GPIO.setmode(GPIO.BOARD)
GPIO_TRIGGER1 = 22
GPIO_ECHO1    = 18
GPIO_TRIGGER3 = 12
GPIO_ECHO3    = 19
GPIO_TRIGGER4 = 16
GPIO_ECHO4    = 21
GPIO_TRIGGER2 = 24
GPIO_ECHO2    = 23
Motor1A = 7
Motor1B = 8
Motor2A = 11
Motor2B = 13
tt=2
trace=[(0,0)]
lw=[]
rw=[]
x,y=0,0
GPIO.setwarnings(False)
GPIO.setup(Motor1A,GPIO.OUT)
GPIO.setup(Motor1B,GPIO.OUT)
GPIO.setup(Motor2A,GPIO.OUT)
GPIO.setup(Motor2B,GPIO.OUT)
GPIO.setup(GPIO_TRIGGER1,GPIO.OUT)  # Trigger
GPIO.setup(GPIO_ECHO1,GPIO.IN)      # Echo
GPIO.output(GPIO_TRIGGER1, False)
GPIO.setup(GPIO_TRIGGER2,GPIO.OUT)  # Trigger
GPIO.setup(GPIO_ECHO2,GPIO.IN)      # Echo
GPIO.output(GPIO_TRIGGER2, False)
GPIO.setup(GPIO_TRIGGER3,GPIO.OUT)  # Trigger
GPIO.setup(GPIO_ECHO3,GPIO.IN)      # Echo
GPIO.output(GPIO_TRIGGER3, False)
GPIO.setup(GPIO_TRIGGER4,GPIO.OUT)  # Trigger
GPIO.setup(GPIO_ECHO4,GPIO.IN)      # Echo
GPIO.output(GPIO_TRIGGER4, False)
print("set")
def measure(mode):
    if(mode=="F"):
        # set Trigger to HIGH
        GPIO.output(GPIO_TRIGGER1, True)
     
        # set Trigger after 0.01ms to LOW
        time.sleep(0.00001)
        GPIO.output(GPIO_TRIGGER1, False)
     
        StartTime = time.time()
        StopTime = time.time()
     
        # save StartTime
        while GPIO.input(GPIO_ECHO1) == 0:
            StartTime = time.time()
     
        # save time of arrival
        while GPIO.input(GPIO_ECHO1) == 1:
            StopTime = time.time()
     
        # time difference between start and arrival
        TimeElapsed = StopTime - StartTime
        # multiply with the sonic speed (34300 cm/s)
        # and divide by 2, because there and back
        distance = (TimeElapsed * 34300) / 2
        return distance
    elif(mode=="B"):
        # set Trigger to HIGH
        GPIO.output(GPIO_TRIGGER2, True)
     
        # set Trigger after 0.01ms to LOW
        time.sleep(0.00001)
        GPIO.output(GPIO_TRIGGER2, False)
     
        StartTime = time.time()
        StopTime = time.time()
     
        # save StartTime
        while GPIO.input(GPIO_ECHO2) == 0:
            StartTime = time.time()
     
        # save time of arrival
        while GPIO.input(GPIO_ECHO2) == 1:
            StopTime = time.time()
     
        # time difference between start and arrival
        TimeElapsed = StopTime - StartTime
        # multiply with the sonic speed (34300 cm/s)
        # and divide by 2, because there and back
        distance = (TimeElapsed * 34300) / 2
        return distance
    elif(mode=="L"):
        # set Trigger to HIGH
        GPIO.output(GPIO_TRIGGER3, True)
     
        # set Trigger after 0.01ms to LOW
        time.sleep(0.00001)
        GPIO.output(GPIO_TRIGGER3, False)
     
        StartTime = time.time()
        StopTime = time.time()
     
        # save StartTime
        while GPIO.input(GPIO_ECHO3) == 0:
            StartTime = time.time()
     
        # save time of arrival
        while GPIO.input(GPIO_ECHO3) == 1:
            StopTime = time.time()
     
        # time difference between start and arrival
        TimeElapsed = StopTime - StartTime
        # multiply with the sonic speed (34300 cm/s)
        # and divide by 2, because there and back
        distance = (TimeElapsed * 34300) / 2
        return distance
    elif(mode=="R"):
        # set Trigger to HIGH
        GPIO.output(GPIO_TRIGGER4, True)
     
        # set Trigger after 0.01ms to LOW
        time.sleep(0.00001)
        GPIO.output(GPIO_TRIGGER4, False)
     
        StartTime = time.time()
        StopTime = time.time()
     
        # save StartTime
        while GPIO.input(GPIO_ECHO4) == 0:
            StartTime = time.time()
     
        # save time of arrival
        while GPIO.input(GPIO_ECHO4) == 1:
            StopTime = time.time()
     
        # time difference between start and arrival
        TimeElapsed = StopTime - StartTime
        # multiply with the sonic speed (34300 cm/s)
        # and divide by 2, because there and back
        distance = (TimeElapsed * 34300) / 2
        return distance
    
def backward():
    GPIO.output(Motor1A,GPIO.HIGH)
    GPIO.output(Motor1B,GPIO.LOW)
    GPIO.output(Motor2A,GPIO.HIGH)
    GPIO.output(Motor2B,GPIO.LOW)
    global tt
    if(tt==3):
        tt=1
    elif(tt==4):
        tt=2
    elif(tt==1):
        tt=3
    elif(tt==2):
        tt=4
def forward():
    GPIO.output(Motor1A,GPIO.LOW)
    GPIO.output(Motor1B,GPIO.HIGH)
    GPIO.output(Motor2A,GPIO.LOW)
    GPIO.output(Motor2B,GPIO.HIGH)
    global x,y,tt
    if(tt==2):
        y=y+1
        trace.append((x,y))
        lw.append((x-l,y))
        rw.append((x+r,y))
    if(tt==1):
        x=x+1
        trace.append((x,y))
        lw.append((x,y+l))
        rw.append((x,y-r))
    if(tt==3):
        x=x-1
        trace.append((x,y))
        lw.append((x,y-l))
        rw.append((x,y+r))
    if(tt==4):
        y=y-1
        trace.append((x,y))
        lw.append((x-r,y))
        rw.append((x+l,y))
def left():
    GPIO.output(Motor1A,GPIO.LOW)
    GPIO.output(Motor1B,GPIO.HIGH)
    GPIO.output(Motor2A,GPIO.HIGH)
    GPIO.output(Motor2B,GPIO.LOW)
    global tt
    if(tt==3):
        tt=4
    elif(tt==4):
        tt=1
    elif(tt==1):
        tt=2
    elif(tt==2):
        tt=3
  ##print(coord)
def right():
    GPIO.output(Motor1A,GPIO.HIGH)
    GPIO.output(Motor1B,GPIO.LOW)
    GPIO.output(Motor2A,GPIO.LOW)
    GPIO.output(Motor2B,GPIO.HIGH)
    global tt
    if(tt==3):
        tt=2
    elif(tt==4):
        tt=3
    elif(tt==1):
        tt=4
    elif(tt==2):
        tt=1
try:
    while True:
    dist=[measure("F"),measure("B"),measure("L"),measure("R")]
    time.sleep(0.5)
    if dist[0] < 25:
       if(dist[2]<dist[3]):
           right()
       elif(dist[2]>dist[3]):
           left()
    else:
       forward()
    if dist[2] < 15:
       if(dist[2]>dist[3]):
           forward()
       elif(dist[2]<dist[3]):
           right()
    if dist[3] < 15:
       if(dist[3]>dist[2]):
           forward()
       elif(dist[3]<dist[2]):
           left()

    plt.plot(*zip(*trace))
    plt.plot(*zip(*lw))

    plt.plot(*zip(*rw))
    plt.show()

except KeyboardInterrupt:
  GPIO.cleanup()
