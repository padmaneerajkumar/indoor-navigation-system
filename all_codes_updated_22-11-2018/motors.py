import RPi.GPIO as GPIO
from time import sleep
GPIO.setmode(GPIO.BOARD)

Motor1A =37
Motor1B = 35
Motor2A = 8
Motor2B = 7

GPIO.setwarnings(False)
GPIO.setup(Motor1A,GPIO.OUT)
GPIO.setup(Motor1B,GPIO.OUT)
GPIO.setup(Motor2A,GPIO.OUT)
GPIO.setup(Motor2B,GPIO.OUT)

def backward():
    GPIO.output(Motor1B,GPIO.LOW)
    GPIO.output(Motor1A,GPIO.HIGH)
    GPIO.output(Motor2B,GPIO.HIGH)
    GPIO.output(Motor2A,GPIO.LOW)

def left():
    GPIO.output(Motor1A,GPIO.LOW)
    GPIO.output(Motor1B,GPIO.LOW)
    GPIO.output(Motor2A,GPIO.LOW)
    GPIO.output(Motor2B,GPIO.LOW)
backward()
left()
