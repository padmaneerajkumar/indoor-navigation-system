import random
import pygame
import sys
import time
import RPi.GPIO as GPIO
from time import sleep
GPIO.setmode(GPIO.BOARD)
GPIO_TRIGGER1 = 22
GPIO_ECHO1    = 18
GPIO_TRIGGER3 = 12
GPIO_ECHO3    = 19
GPIO_TRIGGER4 = 16
GPIO_ECHO4    = 21
GPIO_TRIGGER2 = 24
GPIO_ECHO2    = 23
Motor1A = 7
Motor1B = 8
Motor2A = 11
Motor2B = 13
coord=[(50,50)]
distl=[]
distf=[]
distb=[]
distr=[]
x=50
y=50
GPIO.setup(Motor1A,GPIO.OUT)
GPIO.setup(Motor1B,GPIO.OUT)
GPIO.setup(Motor2A,GPIO.OUT)
GPIO.setup(Motor2B,GPIO.OUT)
GPIO.setup(GPIO_TRIGGER1,GPIO.OUT)  # Trigger
GPIO.setup(GPIO_ECHO1,GPIO.IN)      # Echo
GPIO.output(GPIO_TRIGGER1, False)
GPIO.setup(GPIO_TRIGGER2,GPIO.OUT)  # Trigger
GPIO.setup(GPIO_ECHO2,GPIO.IN)      # Echo
GPIO.output(GPIO_TRIGGER2, False)
GPIO.setup(GPIO_TRIGGER3,GPIO.OUT)  # Trigger
GPIO.setup(GPIO_ECHO3,GPIO.IN)      # Echo
GPIO.output(GPIO_TRIGGER3, False)
GPIO.setup(GPIO_TRIGGER4,GPIO.OUT)  # Trigger
GPIO.setup(GPIO_ECHO4,GPIO.IN)      # Echo
GPIO.output(GPIO_TRIGGER4, False)
print("set")
pygame.init()
red = (255,0,0)
black = (0,0,0)
white=(255,255,255)
clock = pygame.time.Clock()
screen = pygame.display.set_mode((640,480))
img = pygame.image.load('bot.jpeg')
img = pygame.transform.scale(img, (40,50))
def measure(mode):
    if(mode=="F"):
        # set Trigger to HIGH
        GPIO.output(GPIO_TRIGGER1, True)
     
        # set Trigger after 0.01ms to LOW
        time.sleep(0.00001)
        GPIO.output(GPIO_TRIGGER1, False)
     
        StartTime = time.time()
        StopTime = time.time()
     
        # save StartTime
        while GPIO.input(GPIO_ECHO1) == 0:
            StartTime = time.time()
     
        # save time of arrival
        while GPIO.input(GPIO_ECHO1) == 1:
            StopTime = time.time()
     
        # time difference between start and arrival
        TimeElapsed = StopTime - StartTime
        # multiply with the sonic speed (34300 cm/s)
        # and divide by 2, because there and back
        distance = (TimeElapsed * 34300) / 2
        return distance
    elif(mode=="B"):
        # set Trigger to HIGH
        GPIO.output(GPIO_TRIGGER2, True)
     
        # set Trigger after 0.01ms to LOW
        time.sleep(0.00001)
        GPIO.output(GPIO_TRIGGER2, False)
     
        StartTime = time.time()
        StopTime = time.time()
     
        # save StartTime
        while GPIO.input(GPIO_ECHO2) == 0:
            StartTime = time.time()
     
        # save time of arrival
        while GPIO.input(GPIO_ECHO2) == 1:
            StopTime = time.time()
     
        # time difference between start and arrival
        TimeElapsed = StopTime - StartTime
        # multiply with the sonic speed (34300 cm/s)
        # and divide by 2, because there and back
        distance = (TimeElapsed * 34300) / 2
        return distance
    elif(mode=="L"):
        # set Trigger to HIGH
        GPIO.output(GPIO_TRIGGER3, True)
     
        # set Trigger after 0.01ms to LOW
        time.sleep(0.00001)
        GPIO.output(GPIO_TRIGGER3, False)
     
        StartTime = time.time()
        StopTime = time.time()
     
        # save StartTime
        while GPIO.input(GPIO_ECHO3) == 0:
            StartTime = time.time()
     
        # save time of arrival
        while GPIO.input(GPIO_ECHO3) == 1:
            StopTime = time.time()
     
        # time difference between start and arrival
        TimeElapsed = StopTime - StartTime
        # multiply with the sonic speed (34300 cm/s)
        # and divide by 2, because there and back
        distance = (TimeElapsed * 34300) / 2
        return distance
    elif(mode=="R"):
        # set Trigger to HIGH
        GPIO.output(GPIO_TRIGGER4, True)
     
        # set Trigger after 0.01ms to LOW
        time.sleep(0.00001)
        GPIO.output(GPIO_TRIGGER4, False)
     
        StartTime = time.time()
        StopTime = time.time()
     
        # save StartTime
        while GPIO.input(GPIO_ECHO4) == 0:
            StartTime = time.time()
     
        # save time of arrival
        while GPIO.input(GPIO_ECHO4) == 1:
            StopTime = time.time()
     
        # time difference between start and arrival
        TimeElapsed = StopTime - StartTime
        # multiply with the sonic speed (34300 cm/s)
        # and divide by 2, because there and back
        distance = (TimeElapsed * 34300) / 2
        return distance
    
def backward():
  global x,y
  GPIO.output(Motor1A,GPIO.HIGH)
  GPIO.output(Motor1B,GPIO.LOW)
  GPIO.output(Motor2A,GPIO.HIGH)
  GPIO.output(Motor2B,GPIO.LOW)
  print("backward")
  y=y-1
  coord.append((x,y))
  ##print(coord)
def forward():
  global x,y
  GPIO.output(Motor1A,GPIO.LOW)
  GPIO.output(Motor1B,GPIO.HIGH)
  GPIO.output(Motor2A,GPIO.LOW)
  GPIO.output(Motor2B,GPIO.HIGH)
  print("forward")
  y=y+1
  coord.append((x,y))
  ##print(coord)
def left():
  global x,y
  GPIO.output(Motor1A,GPIO.LOW)
  GPIO.output(Motor1B,GPIO.HIGH)
  GPIO.output(Motor2A,GPIO.HIGH)
  GPIO.output(Motor2B,GPIO.LOW)
  print("left")
  x=x-1
  coord.append((x,y))
  ##print(coord)
def right():
  global x,y
  GPIO.output(Motor1A,GPIO.HIGH)
  GPIO.output(Motor1B,GPIO.LOW)
  GPIO.output(Motor2A,GPIO.LOW)
  GPIO.output(Motor2B,GPIO.HIGH)
  print("right")
  x=x+1
  coord.append((x,y))
  ##print(coord)
try:
  distl.append((coord[-1][0]+int(measure("L")),coord[-1][1]))
  distr.append((coord[-1][0]+int(measure("R")),coord[-1][1]))
  distf.append((coord[-1][0],coord[-1][1]+int(measure("F"))))
  distb.append((coord[-1][0],coord[-1][1]+int(measure("B"))))
  while True:
    ##print(measure("L"))
    ##print(measure("F"))
    ##print(measure("R"))
    ##print(measure("B"))
    dist=(measure("F"),measure("B"),measure("L"),measure("R"))
    
    ##print (dist)
    time.sleep(0.5)
    if dist[0] < 25:
       if(dist[2]<dist[3]):
           right()
       elif(dist[2]>dist[3]):
           left()
    else:
       forward()
    if dist[2] < 15:
       if(dist[2]>dist[3]):
           forward()
       elif(dist[2]<dist[3]):
           right()
    if dist[3] < 15:
       if(dist[3]>dist[2]):
           forward()
       elif(dist[3]<dist[2]):
           left()
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
             pygame.quit(); sys.exit();

    # erase the screen
    screen.fill(black)
    msElapsed = clock.tick(1)
    # draw the updated picture
    # changes the location of the points
    
    distl.append((coord[-1][0]+int(dist[2]),coord[-1][1]))
    distr.append((coord[-1][0]+int(dist[3]),coord[-1][1]))
    distf.append((coord[-1][0],coord[-1][1]+int(dist[0])))
    distb.append((coord[-1][0],coord[-1][1]+int(dist[1])))
    pygame.draw.lines(screen,white,False,distl,1)
    pygame.draw.lines(screen,white,False,distr,1)
    pygame.draw.lines(screen,white,False,distf,1)
    pygame.draw.lines(screen,white,False,distb,1)
    screen.blit(img,coord[-1])
    pygame.draw.lines(screen,red,False,coord,5)# redraw the points
##    distl.append((coord[-1][0]+int(dist[2]),coord[-1][1]))
##    distr.append((coord[-1][0]+int(dist[3]),coord[-1][1]))
##    distf.append((coord[-1][0],coord[-1][1]+int(dist[0])))
##    distb.append((coord[-1][0],coord[-1][1]+int(dist[1])))
##    for i in range(0,len(distl)):
##        pygame.draw.circle(screen, white, distl[i], 2)
##        pygame.draw.circle(screen, white, distr[i], 2)
##        pygame.draw.circle(screen, white, distb[i], 2)
##        pygame.draw.circle(screen, white, distf[i], 2)
##    distl.append([coord[-1],(dist[2],coord[-1][1])])
##    distr.append([coord[-1],(dist[3],coord[-1][1])])
##    distf.append([coord[-1],(coord[-1][0],dist[0]])
##    distb.append([coord[-1],(coord[-1][0],dist[0]])
##    for i in range(0,len(distl)):
##      ##print(i)
##      pygame.draw.lines(screen,white,False,distl[i],1)
##      pygame.draw.lines(screen,white,False,distr[i],1)
##      pygame.draw.lines(screen,white,False,distf[i],1)
##      pygame.draw.lines(screen,white,False,distb[i],1)
##      
    # update the screen
    pygame.display.update()
    
except KeyboardInterrupt:
  GPIO.cleanup()
